import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

export default function Login(props) {
  const [loginForm, setLoginForm] = useState({ username: "", password: "" });
  const navigate = useNavigate();

  const handleSubmit = async () => {
    try {
      const backendResponse = await axios.post(
        "https://chapter-platinum-team-2-koet.vercel.app/user/login",
        loginForm
      );
      const token = backendResponse.data.accessToken;
      localStorage.setItem("accessToken", token);
      props.onSuccess();
      navigate("/game");
    } catch (error) {
      console.log(error);
      alert("gagal login");
    }
  };

  return (
    <div>
      <div>
        <label>Username : </label>
        <input
          onChange={(e) =>
            setLoginForm({ ...loginForm, username: e.target.value })
          }
        />
      </div>
      <div>
        <label>Password : </label>
        <input
          onChange={(e) =>
            setLoginForm({ ...loginForm, password: e.target.value })
          }
        />
      </div>
      <div>
        <button onClick={handleSubmit}>SUBMIT</button>
      </div>
    </div>
  );
}
