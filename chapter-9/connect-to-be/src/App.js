import React, { useState } from "react";
import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";
import GamePage from "./pages/GamePage";
import Login from "./pages/Login";

const ProtectedElement = ({ isLoggedIn, children }) => {
  if (!isLoggedIn) {
    return <Navigate to="/login" />;
  } else {
    return children;
  }
};

export default function App() {
  const token = localStorage.getItem("accessToken");
  const [isLoggedIn, setIsLoggedIn] = useState(token ? true : false);

  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<div>Hei there</div>} />
        <Route
          path="/game"
          element={
            <ProtectedElement isLoggedIn={isLoggedIn}>
              <GamePage />
            </ProtectedElement>
          }
        />
        <Route
          path="/login"
          element={<Login onSuccess={() => setIsLoggedIn(true)} />}
        />
      </Routes>
    </BrowserRouter>
  );
}
