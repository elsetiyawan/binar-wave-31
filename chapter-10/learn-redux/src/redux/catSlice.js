import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
  facts: [],
  loading: false,
  errorMessage: "",
};



export const fetchCatFacts = createAsyncThunk(
  'cats/fetchFacts',
  async () => {
      const catFactsDariAPI = await axios.get('https://cat-fact.herokuapp.com/factsx');
      return catFactsDariAPI.data;
  }
);

export const catSlice = createSlice({
  name: "cats",
  initialState,
  reducers: {
    setFacts: (state, action) => {
      state.facts = action.payload;
    },
    tambahkanFacts : (state, action) => {
      state.facts = [...state.facts, action.payload]
    }
  },
  extraReducers: (builder) => {
    builder.addCase(fetchCatFacts.pending, (state) => {
      state.loading = true;
    })
    builder.addCase(fetchCatFacts.fulfilled, (state, action) => {
      state.facts = action.payload
      state.loading = false
    })
    builder.addCase(fetchCatFacts.rejected, (state, payload) => {
      if(payload.error.code === 'ERR_BAD_REQUEST'){
        state.errorMessage = 'salah url woy'      
      } else {
        state.errorMessage = 'error request'      
      }
      state.loading = false
    })
  }
});

export const { setFacts } = catSlice.actions;

export default catSlice.reducer;