import React, { useState } from "react";
import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";
import Form from "./components/Form";
import DataDIri from "./components/DataDIri";
import Belanja from "./components/Belanja";
import Checkout from "./components/Checkout";
import CatFatcs from "./components/CatFatcs";
import CatFatcsThunk from "./components/CatFatcsThunk";
import Homepage from "./components/Homepage";

export default function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route
          path="/"
          element={
            <div style={{ display: "flex", justifyContent: "center" }}>
              <Homepage />
            </div>
          }
        />
        <Route path="/data-diri" element={<DataDIri />} />
        <Route path="/belanja" element={<Belanja />} />
        <Route path="/checkout" element={<Checkout />} />
        <Route path="/cat-facts" element={<CatFatcs />} />
        <Route path="/cat-facts-thunk" element={<CatFatcsThunk />} />
      </Routes>
    </BrowserRouter>
  );
}
