import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { setItem } from "../redux/keranjangSlice";

export default function Belanja() {
  const dispatch = useDispatch();
  const keranjangRedux = useSelector((state) => state.keranjang);

  return (
    <div style={{ padding: 50 }}>
      <div style={{ margin: 20 }}>Halaman Belanja</div>
      <div style={{ margin: 20 }}>
        <input
          onChange={(e) => {
            // setKeranjang(e.target.value);
            dispatch(setItem(e.target.value));
          }}
        />
        <input type="number" onChange={() => {
          // set jumlah didalam redux
        }}/>
      </div>
      <div style={{ margin: 20 }}>
        <Link to="/checkout">Checkout!</Link>
      </div>
      <div style={{ margin: 20 }}>Item : {keranjangRedux.item}</div>
      <div style={{ margin: 20 }}>Jumlah : 5</div>
    </div>
  );
}
