import axios from "axios";
import React, { useEffect, useState } from "react";

function BlogIndex() {
  const [blogs, setBlogs] = useState([]);

  const fetchData = async () => {
    try {
      const blogList = await axios.get(
        "https://jsonplaceholder.typicode.com/posts"
      );
      console.log(blogList.data);
      setBlogs(blogList.data);
    } catch (error) {}
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div style={{ margin: 100, display: "flex", flexWrap: "wrap" }}>
      {blogs.map((blog) => (
        <div
          key={blog.id}
          style={{ width: "100px", border: "1px solid white", margin: 10 }}
        >
          {blog.title}
        </div>
      ))}
    </div>
  );
}

export default BlogIndex;
