const request = require('supertest');
const app = require('../app');

describe('integration test', () => {
    test('/todo should return todo list', async () => {
        const response = await request(app).get('/todo');

        expect(response.statusCode).toBe(200);
        expect(response.body[0].task).toBe('tugas 1')
    })

    test('/todos should return 404', async () => {
        const response = await request(app).get('/todos');

        expect(response.statusCode).toBe(404)
    })

    test('post /todo should create new todo', async () => {
        const response = await request(app).post('/todo').send({ "task": "test integration using supertest" });
        expect(response.statusCode).toBe(200);
        expect(response.body.task).toBe("test integration using supertest");
    })

    test('post /todo should return error when task is not exist', async () => {
        const response = await request(app).post('/todo').send({  });
        console.log(response);
        expect(response.statusCode).toBe(400);
        expect(response.body).toMatchObject({ error: 'request error' });
    })

    // bikin API untuk mendapatkan detail todo
    // { id, task, done } 
    // expected response body
    // {id: 1, task: "tugas 1", done: null}

    test.only('get /todo/{id} should return detail todo', async() => {
        const response = await request(app).get('/todo/1');

        expect(response.statusCode).toBe(201);
        expect(response.body.id).toBe(1)
        expect(response.body.task).toBe("tugas 1")
    })

})