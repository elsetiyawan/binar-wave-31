const express = require('express')
const todoRouter = require('./todo/todo.route')
const { generatePdf } = require('./generatePdf')
const app = express()

app.use(express.json())
app.get('/', (req, res) => {
  return res.send('hello there')
})

app.get('/generate-pdf',async (req, res) => {
  const result = await generatePdf();
  return res.json(result)
})

app.use(todoRouter)

module.exports = app;