const pdf = require('pdf-creator-node');
const fs = require("fs");

const generatePdf = async () => {
    const html = fs.readFileSync("template.html", "utf8");
    const options = {
        format: "Legal",
        orientation: "portrait",
        border: "10mm",
    };

    const gameHistory = [
        {
            tanggal: "01 Juni 2023",
            status: "menang"
        },
        {
            tanggal: "02 Juni 2023",
            status: "kalah"
        },
        {
            tanggal: "02 Juni 2023",
            status: "kalah"
        },
        {
            tanggal: "02 Juni 2023",
            status: "kalah"
        }, {
            tanggal: "02 Juni 2023",
            status: "kalah"
        }, {
            tanggal: "02 Juni 2023",
            status: "kalah"
        }, {
            tanggal: "02 Juni 2023",
            status: "kalah"
        },
    ];

    const document = {
        html: html,
        data: {
            games: gameHistory
        },
        path: "./pdf-generated/output1.pdf",
        type: ""
    }

    try {
        const pdfGenerated = await pdf.create(document, options);
        return pdfGenerated;
    } catch (error) {
        console.log(error);
        return { message: "error" }
    }
};

module.exports = {
    generatePdf
}