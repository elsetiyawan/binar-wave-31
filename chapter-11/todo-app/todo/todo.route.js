const express = require('express')
const todoController = require('./todo.controller')
const todoRouter = express.Router()

todoRouter.get('/todo', todoController.getAllTodos)

todoRouter.post('/todo', todoController.createNewTodo)

todoRouter.get('/todo/:id', todoController.getDetailTask)

module.exports = todoRouter
