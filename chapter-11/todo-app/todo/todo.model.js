const db = require('../db/models')
class TodoModel {
  getAllTodos = async () => {
    const todoDatas = await db.Todo.findAll({raw: true})
    return todoDatas
  }

  createNewTodo = async (task) => {
    const newTask = await db.Todo.create({ task, done: false }).then(
      result => {
        const dataObj = result.get({ plain: true })
        return dataObj
      }
    ).catch(() => {
      return { message: 'create todo error' }
    })
    return newTask
  }

  getTodoDetail = ()=> {
        // cek ke database
    // return data dari database

  }
}

module.exports = new TodoModel()
