const todoModel = require('./todo.model')

class TodoController {
  getAllTodos = async (req, res) => {
    const todos = await todoModel.getAllTodos()
    return res.json(todos)
  }

  createNewTodo = async (req, res) => {
    const { task } = req.body
    if (!task) {
      return res.status(400).json({ error: 'request error' })
    }
    const newTaskCreated = await todoModel.createNewTodo(task)
    return res.json(newTaskCreated)
  }
}

module.exports = new TodoController()
