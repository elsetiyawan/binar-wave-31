const { getAllTodos, createNewTodo } = require("../todo.model")

describe('todo.model', () => {

    test('getAllTodos should return array', async () => {
        const response = await getAllTodos()

        expect(response[0].task).toBe('tugas 1');
    })

    test('createNewTodo should return created todo', async () => {
        const response = await createNewTodo('new task');

        expect(response.task).toBe('new task');
        expect(response.done).toBe(false);
    })
})