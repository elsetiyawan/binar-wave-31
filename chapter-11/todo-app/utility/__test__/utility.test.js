const { addRp, getTotalBayar } = require("../utility");

describe('utility', () => {

    test('add Rp in front of balance', () => {
        // given
        const testSaldo = 5000;
        // Rp. 5000

        // when
        const testedResult = addRp(testSaldo);

        // then
        expect(testedResult).toBe('Rp. 5000,00')
    });

    test('addRp in front of balance should return Masukkan nomor', () => {
        // given
        const testSaldo = "isinya string";

        // when
        const testedResult = addRp(testSaldo);

        // then
        expect(testedResult).toBe('Masukkan nomor')
    });

    // Rp. 5000 bukan Rp. 5000,00

    test('getTotalBayar bebas', () => {
        // given
        const jumlahSabun = 5;
        const hargaSabun = 1000;

        // when
        const totalBayar = getTotalBayar(jumlahSabun, hargaSabun);

        // then
        expect(totalBayar).toBe(5000);
    })

})
