const getTotalBayar = (jumlahItem, hargaItem) => {
    return jumlahItem * hargaItem;
}

const addRp = (saldo) => {
    if(typeof saldo === 'number'){
        return `Rp. ${saldo},00`;
    }else{
        return "Masukkan nomor"
    }
}

module.exports = {
    getTotalBayar,
    addRp
}