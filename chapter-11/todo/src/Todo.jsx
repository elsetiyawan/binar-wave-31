import React, { useEffect, useState } from "react";
// eslint-disable-next-line import/no-extraneous-dependencies
import CardItem from "./components/card";
import axios from "axios";

function Todo() {
  const [error, setError] = useState(null);
  const [todoList, setTodoList] = useState([]);

  const fetchTodo = async () => {
    try {
      const data = await axios.get(
        "https://jsonplaceholder.typicode.com/todos"
      );
      setTodoList(data.data);
    } catch (error) {
      setError("ada error 500");
    }
  };
  useEffect(() => {
    fetchTodo();
  }, []);

  return (
    <div>
      {error ? (
        <div>{error}</div>
      ) : (
        <div style={{ display: "flex", flexWrap: "wrap" }}>
          {todoList.map((todo) => (
            <div key={todo.id} data-testid={`todo-${todo.id}`}>
              <CardItem task={todo.title} done={todo.completed} />
            </div>
          ))}
        </div>
      )}
    </div>
  );
}

export default Todo;
