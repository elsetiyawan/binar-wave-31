import { render, screen } from "@testing-library/react";
import Todo from "./Todo";
import axios from "./__mocks__/axios";

jest.mock('axios');

test('should render todo page', async () => {
    axios.get.mockResolvedValue({
        data: [{
            "userId": 1,
            "id": 1,
            "title": "ini test ngasal",
            "completed": false
        },
        {
            "userId": 1,
            "id": 2,
            "title": "horeee",
            "completed": false
        }]
    })

    render(<Todo />);
    const firstTodo = await screen.findByText(/ini test ngasal/i)

    expect(firstTodo).toBeInTheDocument();

})

test('should render error message when API fail', async () => {
    axios.get.mockRejectedValue()

    render(<Todo />);
    const firstTodo = await screen.findByText(/ada error 500/i)

    expect(firstTodo).toBeInTheDocument();

})
