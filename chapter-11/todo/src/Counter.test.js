import { fireEvent, render, screen } from "@testing-library/react"
import Counter from "./Counter"

test.skip('should render counter page', () => { 
    render(<Counter />);
    const zeroText = screen.getByText("0");
    const theButtons = screen.getAllByRole('button');
    const tambahkanText = screen.getByText(/tambahkan/i);
    const kurangkanText = screen.getByText(/kurangkan/i);

    expect(zeroText).toBeInTheDocument();
    expect(theButtons.length).toBe(2);
    expect(tambahkanText).toBeInTheDocument();
    expect(kurangkanText).toBeInTheDocument();

    fireEvent.click(tambahkanText)
    fireEvent.click(tambahkanText)
    fireEvent.click(tambahkanText)
    fireEvent.click(tambahkanText)
    const oneText = screen.getByText("4");
    expect(oneText).toBeInTheDocument();

    fireEvent.click(kurangkanText)
    fireEvent.click(kurangkanText)
    const terserahNamanya = screen.getByText("2");
    expect(terserahNamanya).toBeInTheDocument();
    
 })