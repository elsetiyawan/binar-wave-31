import { fireEvent, render, screen } from "@testing-library/react"
import Login from "./Login"

test('should render login page correctly', () => {
    render(<Login />);
    const usernameText = screen.getByText('Username :');
    const passwordText = screen.getByText('Password :');
    const inputForm = screen.getAllByRole('textbox');
    const button = screen.getByRole('button');

    expect(usernameText).toBeInTheDocument();
    expect(passwordText).toBeInTheDocument();
    expect(inputForm.length).toBe(2)
    expect(button).toBeTruthy();
    expect(button).toHaveAttribute('disabled')

    const usernameInputForm = screen.getByPlaceholderText(/username/i);
    fireEvent.change(usernameInputForm, { target: { value: "iniusername" } })

    const passwordInputForm = screen.getByPlaceholderText(/password/i);
    fireEvent.change(passwordInputForm, { target: { value: "inipassword" } })
    
    expect(button).not.toHaveAttribute('disabled');

    fireEvent.click(button);

    const iniUsernameText = screen.getByText("iniusername")
    const iniPasswordText = screen.getByText("inipassword")

    expect(iniUsernameText).toBeInTheDocument();
    expect(iniPasswordText).toBeInTheDocument();

})