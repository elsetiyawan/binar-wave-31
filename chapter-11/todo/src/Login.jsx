import React, { useState } from "react";
import InputForLogin from "./components/inputLogin";
import MyButton from "./components/button";

function Login() {
  const [loginData, setLoginData] = useState({ username: "", password: "" });
  const [submitted, setSubmitted] = useState(false);
  return (
    <div>
      <div>
        <label>Username : </label>
        <InputForLogin
          placeholder="username"
          handleChange={(e) => {
            e.preventDefault();
            setLoginData({ ...loginData, username: e.target.value });
          }}
        />
      </div>
      <div>
        <label>Password : </label>
        <InputForLogin
          placeholder="password"
          handleChange={(e) => {
            e.preventDefault();
            setLoginData({ ...loginData, password: e.target.value });
          }}
        />
      </div>
      <MyButton disabled={loginData.username === "" || loginData.password === ""} title="Login" handleClick={() => setSubmitted(true)} />
      {submitted ? (
        <div>
          {loginData.username !== "" ? (
            <div>{loginData.username}</div>
          ) : (
            <div />
          )}
          {loginData.password !== "" ? (
            <div>{loginData.password}</div>
          ) : (
            <div />
          )}
        </div>
      ) : (
        <></>
      )}
    </div>
  );
}

export default Login;
