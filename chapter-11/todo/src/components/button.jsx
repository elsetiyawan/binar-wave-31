import React from "react";

// eslint-disable-next-line react/prop-types
export default function MyButton({ handleClick, title, disabled }) {
  return (
    // eslint-disable-next-line react/prop-types
    <button type="button" onClick={handleClick} disabled={disabled}>
      {title || "default button"}
    </button>
  );
}
