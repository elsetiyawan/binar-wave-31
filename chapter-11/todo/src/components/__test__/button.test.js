import { render, screen } from "@testing-library/react";
import MyButton from "../button";

test('should render button with default button text', () => {
    // 1. render componentnya 
    // 2. mencari componentnya
    // 3. expect 
    render(<MyButton />);
    const textButton = screen.getByText("default button");
    expect(textButton).toBeInTheDocument();
})

test('should render button with title', () => {
    render(<MyButton title="Testing" />);
    const textButton = screen.getByText("Testing");
    expect(textButton).toBeInTheDocument();
})