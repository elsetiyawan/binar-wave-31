import React from "react";

export default function InputForLogin(props) {
  return (
    <input
      placeholder={props.placeholder}
      onChange={props.handleChange}
    />
  );
}
