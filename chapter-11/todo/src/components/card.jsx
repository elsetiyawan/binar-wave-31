import React from 'react';
// eslint-disable-next-line import/no-extraneous-dependencies
import PropTypes from 'prop-types';

function CardItem(props) {
  const { task, done } = props;
  return (
    <div
      style={{
        border: '1px solid black',
        width: '150px',
        margin: '5px',
        borderRadius: 5,
        padding: 10,
        backgroundColor: done ? 'green' : 'lightblue',
      }}
    >
      <div>{task}</div>
      <div>{done ? 'Selesai' : 'Proses'}</div>
    </div>
  );
}

CardItem.propTypes = {
  task: PropTypes.string.isRequired,
  done: PropTypes.bool.isRequired,
};

export default CardItem;
