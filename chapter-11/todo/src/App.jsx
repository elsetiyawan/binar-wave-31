import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Todo from './Todo';
import Counter from './Counter';
import Login from './Login';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<div>Hello there</div>} />
        <Route path="/todo" element={<Todo />} />
        <Route path="/counter" element={<Counter />} />
        <Route path="/login" element={<Login />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
