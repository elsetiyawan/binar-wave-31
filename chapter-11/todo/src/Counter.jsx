import React, { useState } from 'react';
import MyButton from './components/button';

function Counter() {
  const [counter, setCounter] = useState(0);
  return (
    <center>
      <div>{counter}</div>
      <div>
        <MyButton title="Kurangkan" handleClick={() => setCounter(counter - 1)} />
        <MyButton title="Tambahkan" handleClick={() => setCounter(counter + 1)} />
      </div>
    </center>
  );
}

export default Counter;
