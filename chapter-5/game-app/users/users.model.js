const md5 = require("md5");

const userList = [];

class UserModel {
  //   dapatkan semua data
  getAllUser = () => {
    return userList;
  };

  // method check username teregister atau tidak / yes or no / true or false
  isUserRegistered = (dataRequest) => {
    const existData = userList.find((data) => {
      return (
        data.username === dataRequest.username ||
        data.email === dataRequest.email
      );
    });

    if (existData) {
      return true;
    } else {
      return false;
    }
  };

  //   method record new data
  recordNewData = (dataRequest) => {
    userList.push({
      id: 1,
      username: dataRequest.username,
      email: dataRequest.email,
      //   // data password di hash dengan MD5
      password: md5(dataRequest.password),
      fullname: dataRequest.fullname,
    });
  };

  verifyLogin = (username, password) => {
    const dataUser = userList.find((data) => {
      return data.username === username && data.password === md5(password);
    });

    return dataUser;
  };
}

module.exports = new UserModel();
