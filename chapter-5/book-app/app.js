const express = require("express");
const app = express();

app.use(express.json());

let bookList = [];

app.get("/", function (req, res) {
  res.send("Hello World from Nodejs");
});

// API to get all the book list
app.get("/books", function (req, res) {
  res.json(bookList);
});

// API to register a new book
app.post("/books", function (req, res) {
  const data = req.body;
  bookList.push({
    judul: data.judul,
    pengarang: data.pengarang,
  });

  res.json({ message: "Sukses menambahkan buku baru!" });
});

app.listen(3000, () => {
  console.log(`Example app listening on port 3000`);
});
