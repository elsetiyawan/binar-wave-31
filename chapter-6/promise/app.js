// const fs = require("fs");

// const appRunning = async () => {
//   const isiFfile = fs.readFileSync("./readme.txt", "utf-8");
//   console.log(isiFfile)
//   console.log("panggilan 1");
//   console.log("panggilan 2");
//   console.log("panggilan 3");
// };

// appRunning();

// const greeting = (name) => {
//   return new Promise((resolve, reject) => {
//     if (name === "Johan") {
//       resolve("Hi Johan");
//     } else {
//       reject("Bukan Johan");
//     }
//   });
// };

// const theGreet = greeting("Johan");
// console.log(theGreet);
// greeting("Jonathan")
//   .then((res) => console.log(res))
//   .catch((err) => console.log(err));

// const appRunning = async () => {
//   try {
//     const hasil = await greeting("Johan");
//     console.log(hasil);
//   } catch (error) {
//     console.log(err);
//   }
// };

// appRunning();

// const appRunning = async () => {
//   try {
//     const hasil = await greeting("Jonathan");
//     console.log(hasil);
//   } catch (error) {
//     console.log("ada kegagalan proses");
//   }
// };

// appRunning();

const appRunning = async () => {
  console.log("panggilan 1");
  const test = delay();
  console.log(test);
  console.log("panggilan 2");
  console.log("panggilan 3");
};

const delay = async () => {
  await new Promise((resolve) => setTimeout(resolve, 3000));
};

appRunning();
