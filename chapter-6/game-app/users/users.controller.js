const userModel = require("./users.model");

class UserController {
  getAllUsers = async (req, res) => {
    const allUsers = await userModel.getAllUser();
    return res.json(allUsers);
  };

  getSingleUser = async (req, res) => {
    const { idUser } = req.params;

    try {
      const user = await userModel.getSingleUser(idUser);
      if (user === null) {
        return res.json({ message: "user tidak ditemukan" });
      } else {
        return res.json(user);
      }
    } catch (error) {
      res.statusCode = 500;
      return res.json({
        message: `internal server error`,
      });
    }
  };

  updateUserBio = async (req, res) => {
    // dapatkan id user dari request params
    const { idUser } = req.params;

    // dapatkan fullname, address dan phone number dari request body
    const { fullname, address, phoneNumber } = req.body;

    // update user
    // check apakah user bio sudah ada?
    const existUser = await userModel.isUserBioExist(idUser);

    // jika sudah ada, update user bio
    if (existUser) {
      userModel.updateUserBio(idUser, fullname, address, phoneNumber);
    } else {
      // jika belum ada, create user bio
      const data = await userModel.createUserBio(
        idUser,
        fullname,
        address,
        phoneNumber
      );
    }

    return res.json({ message: "user bio is updated" });
  };

  registerUsers = async (req, res) => {
    const dataRequest = req.body;
    //  cek apakah username, email dan password exists
    if (dataRequest.username === undefined || dataRequest.username === "") {
      res.statusCode = 400;
      return res.json({ message: "Username is invalid" });
    }

    if (dataRequest.email === undefined || dataRequest.email === "") {
      res.statusCode = 400;
      return res.json({ message: "Email is invalid" });
    }

    if (dataRequest.password === undefined || dataRequest.password === "") {
      res.statusCode = 400;
      return res.json({ message: "Password is invalid" });
    }

    //   cek apakah username dan email sudah teregistrasi
    const existData = await userModel.isUserRegistered(dataRequest);

    if (existData) {
      return res.json({ message: "Username or email is exist!" });
    }

    //   record data kedalam userList
    userModel.recordNewData(dataRequest);

    return res.json({ message: "New user is recorded" });
  };

  userLogin = async (req, res) => {
    const { username, password } = req.body;
    const dataLogin = await userModel.verifyLogin(username, password);
    if (dataLogin) {
      return res.json(dataLogin);
    } else {
      return res.json({ message: "Invalid credential" });
    }
  };

  getAllGames = async (req, res) => {
    try {
      const data = await userModel.getGameHistories();
      return res.json(data);
    } catch (error) {
      console.log(error);
      return res.send("ada error di query db");
    }
  };
}

module.exports = new UserController();
