const express = require("express");
const userRouter = express.Router();
const userController = require("./users.controller");
const authMiddleware = require("../middleware/authMiddleware");
const { checkSchema } = require("express-validator");
const schemaValidation = require("../middleware/schemaValidation");
const { registrationSchema } = require("./users.schema");

userRouter.get("/", authMiddleware, userController.getAllUsers);

userRouter.post(
  "/registration",
  checkSchema(registrationSchema),
  schemaValidation,
  userController.registerUsers
);

userRouter.post("/login", userController.userLogin);

userRouter.get("/detail/:idUser", authMiddleware, userController.getSingleUser);

userRouter.put("/detail/:idUser", authMiddleware, userController.updateUserBio);

userRouter.get("/games", authMiddleware, userController.getAllGames);

module.exports = userRouter;
