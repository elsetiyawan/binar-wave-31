const md5 = require("md5");
const db = require("../db/models");
const { Op } = require("sequelize");

const userList = [];

class UserModel {
  //   dapatkan semua data
  getAllUser = async () => {
    const dataUsers = await db.User.findAll({
      include: [db.UserBio, db.GameHistory],
    });
    // SELECT * FROM users
    return dataUsers;
  };

  getSingleUser = async (idUser) => {
    return await db.User.findOne({ where: { id: idUser } });
  };

  updateUserBio = async (idUser, fullname, address, phoneNumber) => {
    return await db.UserBio.update(
      { fullname: fullname, address: address, phoneNumber: phoneNumber },
      { where: { user_id: idUser } }
    );
  };

  // method check username teregister atau tidak / yes or no / true or false
  isUserRegistered = async (dataRequest) => {
    const existData = await db.User.findOne({
      where: {
        [Op.or]: [
          { username: dataRequest.username },
          { email: dataRequest.email },
        ],
      },
    });
    if (existData) {
      return true;
    } else {
      return false;
    }
  };

  //   method record new data
  recordNewData = (dataRequest) => {
    // INSERT INTO table () values ()
    db.User.create({
      username: dataRequest.username,
      email: dataRequest.email,
      //   // data password di hash dengan MD5
      password: md5(dataRequest.password),
      fullname: dataRequest.fullname,
    });
  };

  verifyLogin = async (username, password) => {
    const dataUser = await db.User.findOne({
      where: { username: username, password: md5(password) },
      attributes: { exclude: ["password"] },
      raw: true,
    });

    return dataUser;
  };

  getGameHistories = async () => {
    return await db.GameHistory.findAll({
      include: [db.User],
      where: { user_id: 1 },
    });
  };

  isUserBioExist = async (idUser) => {
    const existData = await db.UserBio.findOne({
      where: {
        user_id: idUser,
      },
    });
    if (existData) {
      return true;
    } else {
      return false;
    }
  };

  createUserBio = async (idUser, fullname, address, phoneNumber) => {
    return await db.UserBio.create({
      fullname: fullname,
      address,
      phoneNumber,
      user_id: idUser,
    });
  };

  upsertUserBio = async (idUser, fullname, address, phoneNumber) => {
    return await db.UserBio.upsert({
      id: idUser,
      fullname,
      address,
      phoneNumber,
      user_id: idUser,
    });
  };
}

module.exports = new UserModel();
