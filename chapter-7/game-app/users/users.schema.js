const registrationSchema = {
  email: { isEmail: true },
  username: { isString: true },
  password: {
    errorMessage: "Password should be strong",
  },
};

const loginSchema = {
  email: { isEmail: true },
  password: {
    errorMessage: "Password should be strong",
  },
};

module.exports = {
  registrationSchema,
  loginSchema,
};
