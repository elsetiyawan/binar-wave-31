import { Navigate, Outlet, Route, Routes } from "react-router-dom";
import Dashboard from "./pages/Dashboard";
import Login from "./pages/Login";
import Registration from "./pages/Registration";
import Calculator from "./pages/Calculator/Calculator";
import Layout from "./components/Layout";
import BelajarState from "./pages/BelajarState";
import SinglePost from "./pages/SinglePost";
import Posts from "./pages/Posts";
import Post from "./pages/Post";
import Blog from "./pages/Blog";

const App = () => {
  return (
    <Routes>
      <Route path="/home" element={<div>Ini halaman home tanpa layout</div>} />
      <Route path="/blog" element={<Blog />} />

      <Route path="/posts" element={<Posts />} />
      <Route path="/posts/:id" element={<Post />} />
      <Route path="/posts/:id/:userId" element={<Post />} />
      <Route path="/single-post" element={<SinglePost />} />
      <Route path="/belajar-state" element={<BelajarState />} />
      <Route path="/" element={<Layout />}>
        <Route
          path=""
          element={<div>Ini adalah root dengan path kosong</div>}
        />
        <Route
          path="/dashboard"
          element={
            <div>
              Ini adalah dashboard yang bisa diakses karena ada accessToken
              dalam local storage
            </div>
          }
        />
        <Route path="/login" element={<Login />} />
        <Route path="/registration" element={<Registration />} />
        <Route path="*" element={<div>Halaman 404</div>} />
      </Route>
    </Routes>
  );
};

export default App;
