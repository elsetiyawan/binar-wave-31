import { Button as AntdButton } from "antd";
import { ThunderboltOutlined } from '@ant-design/icons';

const Button = (props) => {
  let buttonTitle = props.title ?? "Klik ME!";
  let buttonColor = props.color ?? "black";

  // props.title ?? "Klik ME!";
  // if(props.title){
  //   props.title
  // }else{
  //   "Klik ME!"
  // }


  return (
    <AntdButton
      style={{
        paddingRight: "20px",
        paddingLeft: "20px",
        backgroundColor: buttonColor,
      }}
      danger
      type="primary"
      // style={{ backgroundColor: "red", padding: "10px", borderRadius: "10px" }}
      onClick={props.handleClick}
      size="small"
      // loading
      // disabled
      icon={<ThunderboltOutlined />}
    >
      {buttonTitle}
    </AntdButton>
  );
};

export default Button;
