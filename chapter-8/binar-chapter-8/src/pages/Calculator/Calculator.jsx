import React, { useState } from "react";
import Button from "../../components/Button";
import "./Calculator.css";

export default function Calculator(props) {
  const [nilai, setNilai] = useState(0);
  const [name, setName] = useState("");
  const [password, setPassword] = useState();

  return (
    <center style={{ marginTop: "10px" }}>
      <div style={{ backgroundColor: nilai !== 0 ? "red" : "yellow" }}>
        <h3>{nilai}</h3>
      </div>

      {/* <div className={nilai !== 0 ? "backgroundNumber" : "backgroundYellow"}>
        <h1>{nilai}</h1>
      </div> */}
      <div style={{ marginBottom: "10px", padding: "20px" }}>
        <input
          className="form-control"
          onChange={(e) => {
            setName(e.target.value);
          }}
        />
      </div>
      <div>name : {name}</div>
      <hr />
      <div style={{ marginBottom: "10px" }}>
        <input
          onChange={(e) => {
            setPassword(e.target.value);
          }}
        />
      </div>
      <div> password : {password}</div>

      <Button
        title="Kurang"
        color="green"
        handleClick={() => {
          if (nilai > 0) {
            setNilai(nilai - 1);
          }
        }}
      />
      <Button
        title="Tambah"
        color="orange"
        handleClick={() => {
          setNilai(nilai + 1);
        }}
      />
    </center>
  );
}
