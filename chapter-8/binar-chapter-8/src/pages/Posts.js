import axios from "axios";
import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";

export default function Posts() {
  const [posts, setPosts] = useState([]);

  useEffect(() => {

    const fetchPost = async () => {
      const dataPosts = await axios.get(
        "https://jsonplaceholder.typicode.com/posts"
      );
      setPosts(dataPosts.data)
    };

    fetchPost();
  }, []);

  return (
    <div style={{ display: "flex", padding: "20px", flexWrap: "wrap" }}>
      {posts.map((post) => {
        return (
          <Link
            to={`/users/${post.userId}`}
            style={{ textDecoration: "none" }}
            key={post.id}
          >
            <div
              style={{
                padding: "10px",
                border: "1px solid lightgreen",
                width: "150px",
              }}
            >
              <div>
                <img
                  width={100}
                  src="https://akcdn.detik.net.id/community/media/visual/2022/07/06/pratama-arhan_43.jpeg?w=700&q=90"
                />
              </div>
              <div style={{ fontWeight: 600 }}>{post.title}</div>
              <div>{post.id}</div>
            </div>
          </Link>
        );
      })}

      {/* <div style={{ padding: "10px", border: "1px solid lightgreen" }}>
        <div>
          <img
            width={200}
            src="https://akcdn.detik.net.id/community/media/visual/2022/07/06/pratama-arhan_43.jpeg?w=700&q=90"
          />
        </div>
        <div style={{ fontWeight: 600 }}>{posts[0].title}</div>
        <div>{posts[0].userId}</div>
      </div>

      <div style={{ padding: "10px", border: "1px solid lightgreen" }}>
        <div>
          <img
            width={200}
            src="https://akcdn.detik.net.id/community/media/visual/2022/07/06/pratama-arhan_43.jpeg?w=700&q=90"
          />
        </div>
        <div style={{ fontWeight: 600 }}>{posts[1].title}</div>
        <div>{posts[1].userId}</div>
      </div> */}
    </div>
  );
}
