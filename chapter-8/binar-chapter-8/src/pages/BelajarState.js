import React, { useState } from "react";

export default function BelajarState() {
  const [pilihan, setPilihan] = useState("");

  return (
    <div style={{ display: "flex" }}>
      <div
        style={{
          border: "1px solid red",
          cursor: "pointer",
          borderRadius: 5,
          margin: 20,
          width: 200,
          height: 50,
          backgroundColor: pilihan === "rock" ? "red" : "pink",
        }}
        onClick={() => {
          if (pilihan === "") {
            setPilihan("rock");
          }
        }}
      >
        Rock
      </div>
      <div
        style={{
          border: "1px solid red",
          cursor: "pointer",
          borderRadius: 5,
          margin: 20,
          width: 200,
          height: 50,
          backgroundColor: "pink",
          backgroundColor: pilihan === "paper" ? "red" : "pink",
        }}
        onClick={() => {
          if (pilihan === "") {
            setPilihan("paper");
          }
        }}
      >
        Paper
      </div>
      <div
        style={{
          border: "1px solid red",
          cursor: "pointer",
          borderRadius: 5,
          margin: 20,
          width: 200,
          height: 50,
          backgroundColor: "pink",
          backgroundColor: pilihan === "scissors" ? "red" : "pink",
        }}
        onClick={() => {
          if (pilihan === "") {
            setPilihan("scissors");
          }
        }}
      >
        Scissors
      </div>
    </div>
  );
}
